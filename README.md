[![build status](https://gitlab.com/ya-cha/oer-db-normalization/badges/master/build.svg)](https://gitlab.com/ya-cha/oer-db-normalization/commits/master)

# OER zur Normalisierung von Datenbanken

Dies ist eine Open Educational Resource (OER) zur Normalisierung von Datenbanken.

Das Online-Material finden Sie unter https://ya-cha.gitlab.io/oer-db-normalization.

Die Druckversion finden Sie unter https://ya-cha.gitlab.io/oer-db-normalization/print.html.

Die Präsentation finden Sie unter https://ya-cha.gitlab.io/oer-db-normalization/slides.html.

Ein Quiz über den Inhalt finden Sie unter https://ya-cha.gitlab.io/oer-db-normalization/quiz.html.

## Lizenz

[![Image of Yaktocat](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0/)

Dieses Werk ist lizenziert unter einer [Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz](https://creativecommons.org/licenses/by-sa/4.0/).

Autor: [Jascha Gerles](https://gitlab.com/ya-cha)


## Bearbeiten des Inhalts

Falls Sie Fehler oder Verbesserungen finden, können Sie diese gerne vorschlagen. Dazu haben Sie zwei Möglichkeiten:
- Falls Sie mit Git vertraut sind, können Sie das Projekt klonen mit
  ```sh
  git clone https://gitlab.com/ya-cha/oer-db-normalization.git
  ```
  Das Material nutzt das Framework [Middleman](https://middlemanapp.com/). Bitte nehmen Sie Ihre Änderungen in einem gesonderten Branch vor und erstellen Sie anschließend einen Merge Request.
- Im [GitLab-Projekt](https://gitlab.com/ya-cha/oer-db-normalization/) können Sie durch die Dateien navigieren und Änderungen online erstellen. Alle Änderungen werden vom Autoren durchgesehen, bevor sie veröffentlicht werden.

### Dateiverzeichnis

Die relevanten Dateien für die Bearbeitung des Inhalts finden Sie im Ordner [source](source).

Er ist folgendermaßen Aufgebaut:

| Ordner           | Funktion                                                                                                                                                                              |
|------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [content](source/content)                   | In diesem Ordner befindet sich der Inhalt für alle Unterseiten. Jede Seite hat einen eigenen Unterordner.                                                  |
| [content/tables](source/content/tables)     | Ordner für Tabellen, die im Material angezeigt werden. Sie werden mit <%= table('name') %> eingebunden.                                                    |
| [elements](source/elements)                 | Elemente, die auf verschiedenen Seiten eingebunden werden können.                                                                                          |
| [images](source/images)                     | Zentraler Speicherort für eingebundene Bilder.                                                                                                             |
| [layouts](source/layouts)                   | Layout-Dateien für die verschiedenen Formate                                                                                                               |
| [index.html.haml](source/index.html.haml)   | Einstiegspunkt für die Startseite. An dieser Stelle werden die Inhalte aus dem Ordner `content` geladen. Diese Datei existiert analog für alle Unterseite. |
| [print.html.haml](source/print.html.haml)   | Einstiegspunkt für die Druckversion.                                                                                                                       |
| [quiz.html.haml](source/quiz.html.haml)     | Einstiegspunkt für das Quiz. Der Inhalt des Quiz kann bearbeitet werden unter `content/quiz/_questions.haml`.                                              |
| [slides.html.haml](source/slides.html.haml) | Einstiegspunkt für die Präsentation.                                                                                                                       |
