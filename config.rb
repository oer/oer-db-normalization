###
# Page options, layouts, aliases and proxies
###

# Per-page layout changes:
#
# With no layout
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

set :haml, :format => :html5

# With alternative layout
# page "/path/to/file.html", layout: :otherlayout

# Proxy pages (http://middlemanapp.com/basics/dynamic-pages/)
# proxy "/this-page-has-no-template.html", "/template-file.html", locals: {
#  which_fake_page: "Rendering a fake page with a local variable" }

# General configuration

# Reload the browser automatically whenever files change
configure :development do
  activate :livereload
end

activate :sprockets

if defined? RailsAssets
  RailsAssets.load_paths.each do |path|
    sprockets.append_path path
  end
end

###
# Helpers
###

# Methods defined in the helpers block are available in templates
helpers do
  def definition(title, content)
    partial "elements/definition", locals: { title: title, content: content }
  end

  def source(source, url)
    partial "elements/source", locals: { source: source, url: url }
  end

  def chapter(name, url)
    partial "elements/chapter", locals: { name: name, url: url }
  end

  def table(name)
    partial "content/tables/#{name}"
  end
end

# Build-specific configuration
configure :build do
  set :build_dir, 'public'
  set :base_url, 'oer-db-normalization'
  set :relative_links, true
  activate :relative_assets
  # Minify CSS on build
  activate :minify_css

  # Minify Javascript on build
  activate :minify_javascript
end
